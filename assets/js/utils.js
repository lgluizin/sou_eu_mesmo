var ftap="3298765432";
var total=0;
var i;
var resto=0;
var numPIS=0;
var strResto="";

//Retira barras, pontos, virgulas e traços parenteses.
function removeCaracteres(valor){
  var retorno = valor.replace(/[\\/(). ,:-]+/g, "");

  return retorno;
}
//Formata data no formato (yyyymmdd) para (ddmmyyyy)
function formataDatas(data){
  if (data > 0){
    var dia = data.toString().substr(6);
    var mes = data.toString().substr(4,8).substr(0,2);
    var ano = data.toString().substr(0,4);

    var dataFormatada = (dia + mes + ano);

    return dataFormatada;
  }
}

function completaCpf(cpf){
  var qtdCpf = "00000000000";

  var valor = qtdCpf.length - cpf.length;

  for (i = 0; i < valor; i++) {
      cpf =  "0" + cpf;
  }

  return cpf;
}


function checaPIS(pis)
{

total=0;
resto=0;
numPIS=0;
strResto="";

	numPIS=pis;

	if (numPIS=="" || numPIS==null)
	{
		return false;
	}

	for(i=0;i<=9;i++)
	{
		resultado = (numPIS.slice(i,i+1))*(ftap.slice(i,i+1));
		total=total+resultado;
	}

	resto = (total % 11)

	if (resto != 0)
	{
		resto=11-resto;
	}

	if (resto==10 || resto==11)
	{
		strResto=resto+"";
		resto = strResto.slice(1,2);
	}

	if (resto!=(numPIS.slice(10,11)))
	{
		return false;
	}

	return true;
}

function validarData(obj){
  var data = obj;
      var dia = data.substring(0,2)
      var mes = data.substring(3,5)
      var ano = data.substring(6,10)

      var today = new Date();
      var yyyy = today.getFullYear();

      //Criando um objeto Date usando os valores ano, mes e dia.
      var novaData = new Date(ano,(mes-1),dia);

      var mesmoDia = parseInt(dia,10) == parseInt(novaData.getDate());
      var mesmoMes = parseInt(mes,10) == parseInt(novaData.getMonth())+1;
      var mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear());

      if(parseInt(ano) > yyyy){
        return false;
      }

      if(parseInt(ano) < 1900){
        return false;
      }

      if (!((mesmoDia) && (mesmoMes) && (mesmoAno)))
      {
          return false;
      }
      return true;
  }

//Valida CPF
function validarCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g,'');
    if(cpf == '') return false;
    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
            return false;
    // Valida 1o digito
    add = 0;
    for (i=0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;
    return true;
}

function convertImgToBase64(url, callback, outputFormat){
	var canvas = document.createElement('CANVAS');
	var ctx = canvas.getContext('2d');
	var img = new Image;
	img.crossOrigin = 'Anonymous';
	img.onload = function(){
		canvas.height = img.height;
		canvas.width = img.width;
	  	ctx.drawImage(img,0,0);
	  	var dataURL = canvas.toDataURL(outputFormat || 'image/png');
	  	callback.call(this, dataURL);
        // Clean up
	  	canvas = null;
	};
	img.src = url;
}


function notificationAlert(mensagem, funcaoCallback, titulo, botaoFechar)
{
	if(navigator.notification == undefined)	{
		alert(mensagem);
	} else {
		navigator.notification.alert(mensagem, funcaoCallback, titulo, botaoFechar);
	}
}

//O parâmetro buttonIndexOk serve para indicar o índice do botão que chama o callback para quando não tem suporte para navigator.notification
function notificationConfirm(mensagem, funcaoCallback, titulo, arrBotoes, buttonIndex)
{

	if(navigator.notification == undefined)	{

	    var confirmAcao = confirm(mensagem);

		if (confirmAcao) {
	        funcaoCallback(buttonIndexOk);
		}

	} else {
		navigator.notification.confirm(mensagem, funcaoCallback, titulo, arrBotoes);
	}

}

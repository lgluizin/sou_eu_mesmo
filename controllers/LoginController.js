window.addEventListener('load', function () {
    new FastClick(document.body);
}, false);

//Registra os cliques nos botões da navbar
$("html").on("click", ".navbar .botao_esquerda, .navbar .botao_direita",
  function() {
    var urlClick = $(this).data("url-click");
    if(urlClick !== undefined) {
      location.href = urlClick;
    }
  }
);

var loginController = {
	init: function(){
		window.sessionStorage.clear();

    VMasker(document.getElementById('login_input_text_cpf')).maskPattern("999.999.999-99");

		var button = document.getElementById("btnCadastrar");
		button.addEventListener('click', this.cadastrar, false);

		var btnEntrar = document.getElementById("btnEntrar");
		btnEntrar.addEventListener('click', this.validarAutenticacao, false);

    var btnEsquecerSenha = document.getElementById("btnEsquecerSenha");
    btnEsquecerSenha.addEventListener('click', this.esqueciMinhaSenha, false);



    sessionStorage.novoCadastro = true;
    },
	cadastrar: function(){
    SpinnerDialog.show();
		location.href = "../views/cadastro.html";
    SpinnerDialog.hide();
	},
  esqueciMinhaSenha: function(){
    SpinnerDialog.show();
    location.href = "../views/esqueci_minha_senha.html";
    SpinnerDialog.hide();
  },
	dadosAutenticacao: function(dadosRetornados){
    window.sessionStorage.setItem('autenticado', 'true');
    window.sessionStorage.setItem('logadoPrimeiraVez', 'true');

    location.href = '../views/cadastro.html';


	},
  validarAutenticacao: function(){
    var cpf = removeCaracteres(document.getElementById('login_input_text_cpf').value);
    if (cpf == ''){
      notificationAlert(
        "Informe seu CPF",
        function() {},
        "Atenção",
        "OK"
      );
      return false;
    }else if(!validarCPF(completaCpf(cpf))){
      notificationAlert(
        "Digite um CPF válido",
        function() {},
        "Atenção",
        "OK"
      );
      return false;
    }
    else if (document.getElementById('login_input_text_senha').value == ''){
      notificationAlert(
        "Informe sua SENHA",
        function() {},
        "Atenção",
        "OK"
      );
      return false;
    }

    loginController.autenticar();
    return true;
  },
	autenticar: function(){
	   SpinnerDialog.show();
    var cpf = removeCaracteres(document.getElementById('login_input_text_cpf').value);
    var senha = document.getElementById('login_input_text_senha').value;

		$.ajax(
			{
				url: config.wsUrl + "AutenticacaoMobile",
				type: "POST",
				contentType: " application/json; charset=utf-8",
				dataType: "json",
				data: JSON.stringify({"cpf":cpf, "senha":senha})
			}).done(
				function (data) {
					var dadosRetornados = JSON.parse( data );
          SpinnerDialog.hide();
					loginController.dadosAutenticacao(dadosRetornados);

						//alert(data);
			}).fail(
				function(xhr, textStatus, error){
          SpinnerDialog.hide();
          if (error && error == "Not Found") {
            notificationAlert(
              "Falha ao conectar com o servidor.",
              function() {},
              "Erro",
              "OK"
            );            
          } else {
           notificationAlert(
              "Usuário ou senha incorretos",
              function() {},
              "Atenção",
              "OK"
            ); 
          }          
			  });
		}
  };

var inicialController = {
    // Application Constructor
  init: function() {
    var btnCadastrar = document.getElementById("btnCadastrar");
		btnCadastrar.addEventListener('click', this.cadastrar, false);
		var btnLogin = document.getElementById("btnLogin");
		btnLogin.addEventListener('click', this.login, false);
    },
  cadastrar: function(){
		location.href = '../views/cadastro.html';
  },
	login: function(){
		location.href = '../views/login.html';
	}
};

window.addEventListener('load', function () {
  new FastClick(document.body);
}, false);

var oldPhoto = null;

//Registra os cliques nos botões da navbar
$("html").on("click", ".navbar .botao_esquerda, .navbar .botao_direita",
  function () {
    var urlClick = $(this).data("url-click");
    if (urlClick !== undefined) {
      location.href = urlClick;
    }
  }
);

function populateDB(tx) {
  tx.executeSql('CREATE TABLE IF NOT EXISTS CADASTROS (id integer primary key autoincrement, data, status)');
}


function errorCB(err) {
  ////SpinnerDialog.hide();
  console.log("Error processing SQL: " + err.code);
  console.log("Error processing SQL: " + err.message);
}

function successCB() {
  console.log("success!");
}

//
function insertDB(tx) {
  tx.executeSql('INSERT INTO CADASTROS (data,status) VALUES (?,?)', [sessionStorage.dadosAtual, 0], querySuccess, errorCB);
}

function selectDB(tx) {
  tx.executeSql('SELECT * FROM CADASTROS', [], querySuccess, errorCB);
}


function querySuccess(tx, results) {
  ////SpinnerDialog.hide();
  notificationAlert(
    "Cadastrado com sucesso!",
    function () { },
    "Atenção",
    "OK"
  );
  cadastroController.limparCadastroBasico();
  location.href = "#voltarParaHome";
  sessionStorage.foto = "";
  sessionStorage.cpf = "";
  //location.reload();

}


var db = null;
var connectionStatus = null;
var states;
var cadastroController = {
  // Application Constructor
  init: function () {
    document.getElementById('container_lista_empresas').innerHTML = "";

    document.getElementById('container_lista_empresas_sincronizados').innerHTML = "";
    document.addEventListener("deviceready", this.onDeviceReady, false);
    //this.onDeviceReady();
  },
  onDeviceReady: function () {
    cadastroController.setarMascaraCadastro();
    oldPhoto = document.getElementById('btnFoto').innerHTML;

    var btnFoto = document.getElementById('btnFoto');
    btnFoto.addEventListener('click', cadastroController.capturaFoto, false);

    if (db != null) {
      db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
      db.transaction(populateDB, errorCB, successCB);
    }

    cadastroController.atualizaContadorRecente();
    cadastroController.atualizaContadorSincronizado();
    setInterval(function () {
      connectionStatus = navigator.connection.type;

      states = {};
      states[Connection.UNKNOWN] = 'Unknown connection';
      states[Connection.ETHERNET] = 'Ethernet connection';
      states[Connection.WIFI] = 'WiFi connection';
      states[Connection.CELL_2G] = 'Cell 2G connection';
      states[Connection.CELL_3G] = 'Cell 3G connection';
      states[Connection.CELL_4G] = 'Cell 4G connection';
      states[Connection.CELL] = 'Cell generic connection';
      states[Connection.NONE] = 'No network connection';
    }, 100);

    if (sessionStorage.logadoPrimeiraVez == "true") {
      sessionStorage.logadoPrimeiraVez = "false";
      cadastroController.clickSincronizar();
    }
  },
  sincronizar: function (callback) {
    SpinnerDialog.show();
    var id = 0;
    db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM CADASTROS WHERE status = ?', [0], function (tx, results) {
        if (results.rows.length > 0) {
          for (var i = 0; i < results.rows.length; i++) {
            dadosRetornados = results.rows.item(i).data;
            id = results.rows.item(i).id;

            cadastroController.salvarCadastroWs(dadosRetornados, id, function (status, id) {
              console.log(status);

              if (status == 200) {
                db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
                db.transaction(function (tx) {
                  tx.executeSql('UPDATE CADASTROS SET status = ? WHERE  id = ?', [1, id], null, errorCB);
                }, errorCB);
              } else {
                db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
                db.transaction(function (tx) {
                  tx.executeSql('UPDATE CADASTROS SET status = ? WHERE  id = ?', [2, id], null, errorCB);
                }, errorCB);
              }

              if (i == results.rows.length) {
                callback();
              }

            });
            //},8000);
          }
        } else {
          SpinnerDialog.hide();
          notificationAlert(
            "Você não tem novos cadastros para sincronizar",
            function () { },
            "Atenção",
            "OK"
          );
        }
      });
    }, errorCB);
  },
  salvarCadastroWs: function (data, id, callback) {
    $.ajax(
      {
        url: config.wsUrl + "SalvarCadastroBasico",
        type: "POST",
        contentType: " application/json; charset=utf-8",
        dataType: "json",
        data: data
      }).done(
      function (data) {
        /*var dadosRetornados = JSON.parse( data );
        console.log(dadosRetornados.IdPessoaFisica);
        sessionStorage.idPessoaFisica = dadosRetornados.IdPessoaFisica;
        sessionStorage.novoCadastro = false;
        sessionStorage.autenticado = true;
        location.href= '#continuarParaEmpresa';
        retorno = "true";
        //SpinnerDialog.hide();
          //alert(data);*/
        callback(200, id);
      }).fail(
      function (jqXHR, textStatus, errorThrown) {
        callback(jqXHR.status, id);
        /*    if(jqXHR.status == 409){
              notificationAlert(
                "Não foi possível salvar: CPF já cadastrado",
                function() {},
                "Atenção",
                "OK"
              );
  
            }else{
              notificationAlert(
                "Não foi possível continuar: tente novamente",
                function() {},
                "Atenção",
                "OK"
              );
            }*/
      });
  },
  atualizaContadorRecente: function () {
    db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM CADASTROS where status = ?', [0], function (tx, results) {
        document.getElementById('qtdCadastrosRecentes').innerHTML = results.rows.length;
      });
    }, errorCB);
  },
  atualizaContadorSincronizado: function () {
    db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
    db.transaction(function (tx) {
      tx.executeSql('SELECT * FROM CADASTROS where status = ?', [1], function (tx, results) {
        document.getElementById('qtdCadastrosSincronizados').innerHTML = results.rows.length;
      });
    }, errorCB);
  },
  enviarCadastroBasico: function () {
    //alert(sessionStorage.foto);
    ////SpinnerDialog.show();

    var data = {
      "cpf": sessionStorage.cpf, "nome": "", "data": 0,
      "nis": "", "celular": "", "ddd": "", "email": "sememail@sememail.com",
      "senha": "1234", "novoCadastro": "false", "foto": sessionStorage.foto ? sessionStorage.foto : ""
    };

    sessionStorage.dadosAtual = JSON.stringify(data);

    db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
    db.transaction(insertDB, errorCB);
  },
  VerificarCpf: function (cpf) {
    //SpinnerDialog.show();
    $.ajax(
      {
        url: config.wsUrl + "VerificarCpfApp",
        type: "POST",
        contentType: " application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ "cpf": cpf })
      }).done(
      function (data) {
        //SpinnerDialog.hide();
        notificationConfirm(
          "CPF já cadastrado",
          function (buttonIndex) {
            switch (buttonIndex) {
              case 2:
                //SpinnerDialog.show();
                location.href = "../views/esqueci_minha_senha.html";
                //SpinnerDialog.hide();
                break;
            }
          },
          "Atenção",
          [
            "OK",
            "Esqueci minha senha"
          ],
          0
        );


      }).fail(
      function (jqXHR, textStatus, errorThrown) {
        //SpinnerDialog.hide();

        if (jqXHR.status == 404) {
        } else {
          notificationAlert(
            "Não foi possível continuar: tente novamente",
            function () { },
            "Atenção",
            "OK"
          );
        }
      });
  },
  salvarCadastroBasicoSessao: function () {
    sessionStorage.cpf = removeCaracteres(document.getElementById('cadastro_input_text_cpf').value);
  },
  setarMascaraCadastro: function () {
    VMasker(document.getElementById('cadastro_input_text_cpf')).maskPattern("999.999.999-99");
    // VMasker(document.getElementById('cadastro_input_text_data_de_nascimento')).maskPattern("99/99/9999");
    // VMasker(document.getElementById('cadastro_input_text_nis')).maskPattern("999.99999.99-9");
    // VMasker(document.getElementById('cadastro_input_text_celular')).maskPattern("(99) 999999999");
  },
  login: function () {
    //SpinnerDialog.show();
    location.href = '../views/login.html';
    //SpinnerDialog.hide();
  },
  listarUsuarios: function (id, dadosRetornados) {
    var empresasCarregadas = document.getElementById('container_lista_empresas');
    var novaEmpresa = '<div id="btnUsuarioNaoSincronizado" class="item">' +
      '<div class="nome_da_empresa" id="' + id + '">' + dadosRetornados.cpf + '</div><div id="btnRemoverUsuario" class="icone_excluir"><img alt="" src="../assets/img/icone_excluir.png"></div>' +
      '</div>';
    empresasCarregadas.innerHTML += novaEmpresa;
  },
  listarUsuariosSincronizados: function (id, dadosRetornados) {
    var empresasCarregadas = document.getElementById('container_lista_empresas_sincronizados');
    var novaEmpresa = '<div id="btnSelecionarUsuario" class="item">' +
      '<div class="nome_da_empresa" id="' + id + '">' + dadosRetornados.cpf + '</div>';
    empresasCarregadas.innerHTML += novaEmpresa;
  },
  editar: function (id) {

  },
  deletarSincronizados: function (callback) {
    db.transaction(function (tx) {
      tx.executeSql('DELETE FROM CADASTROS where status = ?', [1], function (tx, results) {
        callback();
      });
    }, errorCB);
  },
  deletar: function (id, status) {
    db.transaction(function (tx) {
      tx.executeSql('DELETE FROM CADASTROS where id = ?', [id], function (tx, results) {
        tx.executeSql('SELECT * FROM CADASTROS where status = ?', [status], function (tx, results) {
          for (var i = 0; i < results.rows.length; i++) {
            var dadosRetornados = JSON.parse(results.rows.item(i).data);
            var id = results.rows.item(i).id;
            cadastroController.listarUsuarios(id, dadosRetornados);
            //console.log("Row = " + i + ", id = " + results.rows.item(i).data);
          }
        });
      });
    }, errorCB);
  },
  limparCadastroBasico: function () {
    //Cadastro
    document.getElementById('cadastro_input_text_cpf').value = "";
    document.getElementById('btnFoto').innerHTML = oldPhoto;
  },
  setarFoto: function (imageData, base) {
    if (imageData) {
      if (base == true) {
        sessionStorage.foto = imageData;
        document.getElementById('cadastro').getElementsByClassName('icone_tirar_foto')[0].style.backgroundImage = "url(data:image/png;base64," + imageData + ")";
      } else {
        sessionStorage.foto = window.btoa(imageData);
        document.getElementById('cadastro').getElementsByClassName('icone_tirar_foto')[0].style.backgroundImage = "url(" + imageData + ")";
      }

      document.getElementById('cadastro').getElementsByClassName('icone_tirar_foto')[0].style.backgroundPosition = "center center";
      document.getElementById('cadastro').getElementsByClassName('icone_tirar_foto')[0].style.backgroundSize = "100% auto";
      //window.sessionStorage.setItem("Foto", )
      document.getElementById('cadastro').getElementsByClassName('label_tirar_foto')[0].innerText = "ALTERAR FOTO";
    }
  },
  capturaFoto: function () {
    navigator.camera.getPicture(
      //SUCCESS
      function (imageData) {
        //  sessionStorage.foto = btoa(url(imageData));

        var imageUrl = imageData;
        sessionStorage.imageData;
        convertImgToBase64(imageUrl, function (base64Img) {
          base64Img = base64Img.replace("data:image/png;base64,", "");
          sessionStorage.foto = base64Img;
        });

        document.getElementById('cadastro').getElementsByClassName('icone_tirar_foto')[0].style.backgroundImage = "url(" + imageData + ")";
        document.getElementById('cadastro').getElementsByClassName('icone_tirar_foto')[0].style.backgroundPosition = "center center";
        document.getElementById('cadastro').getElementsByClassName('icone_tirar_foto')[0].style.backgroundSize = "100% auto";
        //window.sessionStorage.setItem("Foto", )
        document.getElementById('cadastro').getElementsByClassName('label_tirar_foto')[0].innerText = "ALTERAR FOTO";

      },
      //FAIL
      function (message) {
        //alert(message);
      },
      {
        quality: 70,
        targetWidth: 640,
        correctOrientation: true,
        destinationType: Camera.DestinationType.FILE_URI
      }
    );
  },
  visualizarCadastro: function () {
  },
  voltarParaCadastro: function () {
    location.href = '#voltarParaCadastro';
  },
  continuarParaCadastro: function () {
    location.href = "#irParaCadastro";
  },

  continuarParaEmpresa: function () {
    if (this.validarCadastro()) {
      cadastroController.enviarCadastroBasico();
    }
  },
  clickSincronizar: function () {
    if (sessionStorage.autenticado == "true") {
      notificationConfirm(
        "Tem certeza que deseja sincronizar?",
        function (buttonIndex) {
          switch (buttonIndex) {
            case 1:
              if (states[connectionStatus] == "No network connection") {
                notificationAlert(
                  "Você está sem conexão!",
                  function () { },
                  "Atenção",
                  "OK"
                );
              } else {
                if (states[connectionStatus] == "Cell 3G connection" || states[connectionStatus] == "Cell 4G connection") {
                  notificationConfirm(
                    "Você não está conectado a uma rede Wifi, deseja sincronizar mesmo assim?",
                    function (buttonIndex) {
                      switch (buttonIndex) {
                        case 1:
                          SpinnerDialog.show();
                          //  location.href = '../views/login.html';
                          cadastroController.sincronizar(function () {
                            db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
                            db.transaction(function (tx) {
                              tx.executeSql('SELECT * FROM CADASTROS where status = ?', [0], function (tx, results) {
                                if (results.rows.length <= 0) {
                                  cadastroController.atualizaContadorRecente();
                                  cadastroController.atualizaContadorSincronizado();
                                  SpinnerDialog.hide();
                                  notificationAlert(
                                    "Sincronização realizada com sucesso!",
                                    function () { },
                                    "Atenção",
                                    "OK"
                                  );
                                }
                              });
                            });
                          });
                          break;
                      }
                    },
                    "Atenção",
                    [
                      "Sim",
                      "Não"
                    ],
                    0
                  );
                } else if (states[connectionStatus] == "WiFi connection") {
                  SpinnerDialog.show();
                  cadastroController.sincronizar(function () {
                    db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
                    db.transaction(function (tx) {
                      tx.executeSql('SELECT * FROM CADASTROS where status = ?', [0], function (tx, results) {
                        if (results.rows.length <= 0) {
                          cadastroController.atualizaContadorRecente();
                          cadastroController.atualizaContadorSincronizado();
                          SpinnerDialog.hide();
                          notificationAlert(
                            "Sincronização realizada com sucesso!",
                            function () { },
                            "Atenção",
                            "OK"
                          );
                        }
                      });
                    });
                  });
                }
              }
          }
        },
        "Atenção",
        [
          "Sim",
          "Cancelar"
        ],
        0
      );

    } else {
      if (states[connectionStatus] == "No network connection") {
        notificationAlert(
          "Você está sem conexão!",
          function () { },
          "Atenção",
          "OK"
        );
      } else {
        SpinnerDialog.hide();
        location.href = location.href = '../views/login.html';
      }
    }
  },
  validarCadastro: function () {
    var cpf = removeCaracteres(document.getElementById('cadastro_input_text_cpf').value);
    if (document.getElementById('cadastro_input_text_cpf').value == '') {
      notificationAlert(
        "Informe seu CPF",
        function () { },
        "Atenção",
        "OK"
      );
      document.getElementById('cadastro_input_text_cpf').focus();
      return false;
    } else if (!validarCPF(completaCpf(cpf))) {
      notificationAlert(
        "Informe um CPF válido",
        function () { },
        "Atenção",
        "OK"
      );
      document.getElementById('cadastro_input_text_cpf').focus();
      return false;
    }

    cadastroController.salvarCadastroBasicoSessao();
    return true;
  }
};

$("html").on("click", "#btnCadastrar", function () {
  cadastroController.continuarParaCadastro();
});

$("html").on("click", "#btnSincronizar", function () {
  cadastroController.clickSincronizar();


  // if(connectionStatus == "online"){
  //   cadastroController.sincronizar(function(){
  //
  //   });
  // }else{
  //   alert("Você não está conectado em uma rede Wifi");
  // }

});

$("html").on("click", "#btnHome", function () {
  location.href = "#voltarParaHome";
});

$("html").on("click", "#btnRemoverUsuario", function () {
  var id = $(this).parents(".item")[0].children[0].id;

  notificationConfirm(
    "Você tem certeza que deseja remover esse cadastro?",
    function (buttonIndex) {
      switch (buttonIndex) {
        case 1:
          cadastroController.deletar(id, 0);
          document.getElementById('container_lista_empresas').innerHTML = "";
          break;
      }
    },
    "Atenção",
    [
      "Sim",
      "Não"
    ],
    0
  );
});


$("html").on("click", "#btnVisualizar", function () {
  //SpinnerDialog.show();
  location.href = "#visualizarCadastros";
  db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
  db.transaction(function (tx) {
    tx.executeSql('SELECT * FROM CADASTROS where status = ?', [0], function (tx, results) {
      for (var i = 0; i < results.rows.length; i++) {
        var dadosRetornados = JSON.parse(results.rows.item(i).data);
        var id = results.rows.item(i).id;
        cadastroController.listarUsuarios(id, dadosRetornados);
        //console.log("Row = " + i + ", id = " + results.rows.item(i).data);
      }

      //SpinnerDialog.hide();
    });
  }, errorCB);
  //
});

$("html").on("click", "#btnVisualizarSincronizados", function () {
  //SpinnerDialog.show();
  location.href = "#visualizarCadastrosSincronizados";
  document.getElementById('container_lista_empresas_sincronizados').innerHTML = "";
  db = window.openDatabase("soueumesmo", "1.0", "SouEuMesmo DB", 1000000);
  db.transaction(function (tx) {
    tx.executeSql('SELECT * FROM CADASTROS WHERE status = ?', [1], function (tx, results) {
      for (var i = 0; i < results.rows.length; i++) {
        var dadosRetornados = JSON.parse(results.rows.item(i).data);
        var id = results.rows.item(i).id;
        cadastroController.listarUsuariosSincronizados(id, dadosRetornados);
        //console.log("Row = " + i + ", id = " + results.rows.item(i).data);
      }
      //SpinnerDialog.hide();
    });
  }, errorCB);

});

$(document).ready(function () {
  $("#cadastro_input_text_cpf").blur(function (e) {
    var cpf = removeCaracteres(document.getElementById('cadastro_input_text_cpf').value);
    if (sessionStorage.cpf != cpf) {
      if (!validarCPF(completaCpf(cpf))) {
        notificationAlert(
          "Informe um CPF válido",
          function () { },
          "Atenção",
          "OK"
        );
        return false;
      } else {
        cadastroController.VerificarCpf(cpf)
      }
    }
  });

});

$("html").on("click", "#btnContinuarParaEmpresa", function () {
  cadastroController.continuarParaEmpresa();
});

$("html").on("click", "#btnVoltarParaLogin", function () {
  notificationConfirm(
    "Tem certeza que deseja sair?",
    function (buttonIndex) {
      switch (buttonIndex) {
        case 1:
          navigator.app.exitApp();
          break;
      }
    },
    "Atenção",
    [
      "Sim",
      "Não"
    ],
    0
  );
});


$("html").on("click", "#btnLimpar", function () {
  cadastroController.deletarSincronizados(function () {
    document.getElementById('container_lista_empresas_sincronizados').innerHTML = "";
    //retirar da grid
    //location.reload();
  });
});

//Primeira section, cadastro básico.
var cadastroTela = document.getElementById('cadastroTela').innerHTML;
cadastroTela = $($.parseHTML(cadastroTela));

var home = document.getElementById('home').innerHTML;
home = $($.parseHTML(home));

var visualizar = document.getElementById('visualizarCadastros').innerHTML;
visualizar = $($.parseHTML(visualizar));

var visualizarSincronizados = document.getElementById('cadastrosSincronizados').innerHTML;
visualizarSincronizados = $($.parseHTML(visualizarSincronizados));
//As sections são injetadas no elemento 'container' que aqui é instanciado com a lib PageSlider.
var slider = new PageSlider($("#container"));
$(window).on('hashchange', route);

//Rotas que definem para qual section será redirecionado.
function route(event) {
  var page,
    hash = window.location.hash;

  if (hash === "#continuarParaCadastro") {
    page = cadastroTela;
    slider.slidePageFrom($(page), "left");

  } else if (hash === "#voltarParaCadastro") {
    page = cadastroTela;
    slider.slidePageFrom($(page), "left");

  } else if (hash === "#irParaCadastro") {
    page = cadastroTela;
    slider.slidePageFrom($(page), "right");
    cadastroController.init();
  } else if (hash === "#visualizarCadastros") {
    page = visualizar;
    slider.slidePageFrom($(page), "right");
    cadastroController.init();
  } else if (hash === "#visualizarCadastrosSincronizados") {
    page = visualizarSincronizados;
    slider.slidePageFrom($(page), "right");
    cadastroController.init();
  } else if (hash === "#voltarParaHome") {

    page = home;
    slider.slidePageFrom($(page), "left");
    cadastroController.atualizaContadorRecente();
    cadastroController.atualizaContadorSincronizado();
  } else if (hash === "#irParaVisualizar") {
    page = home;
    slider.slidePageFrom($(page), "right");
  } else if (hash === "#irParaSincronizacao") {
    page = home;
    slider.slidePageFrom($(page), "right");
  } else {
    page = home;
    slider.slidePageFrom($(page), "left");
  }
}

route();

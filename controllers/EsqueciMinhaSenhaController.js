var esqueciMinhaSenhaController = {
  init: function(){
    VMasker(document.getElementById('esqueci_minha_senha_input_text_cpf')).maskPattern("999.999.999-99");

    var btnRecuperar = document.getElementById("btnRecuperar");
    btnRecuperar.addEventListener('click', this.validarRecuperar, false);

    var btnLogin = document.getElementById("btnLogin");
    btnLogin.addEventListener('click', this.login, false);

    var btnPesquisar = document.getElementById("btnPesquisar");
    btnPesquisar.addEventListener('click', this.pesquisar, false);
  },
  validar: function(){
    var cpf = removeCaracteres(document.getElementById('esqueci_minha_senha_input_text_cpf').value);

    if(cpf == ''){
      notificationAlert(
        "Informe um CPF",
        function() {},
        "Atenção",
        "OK"
      );
      return false;
    }
    if(!validarCPF(completaCpf(cpf))){
      notificationAlert(
        "Informe um CPF válido",
        function() {},
        "Atenção",
        "OK"
      );
			return false;
    }

    esqueciMinhaSenhaController.retornarEmail();
    return true;
  },
  validarRecuperar: function(){
    var cpf = removeCaracteres(document.getElementById('esqueci_minha_senha_input_text_cpf').value);

    if(cpf == ''){
      notificationAlert(
        "Informe um CPF",
        function() {},
        "Atenção",
        "OK"
      );
      return false;
    }
    if(!validarCPF(completaCpf(cpf))){
      notificationAlert(
        "Informe um CPF válido",
        function() {},
        "Atenção",
        "OK"
      );
      return false;
    }

    esqueciMinhaSenhaController.recuperar(cpf);
    return true;
  },
  recuperar: function(cpf){
    SpinnerDialog.show();
    $.ajax(
      {
        url: config.wsUrl + "RecuperarSenha",
        type: "POST",
        contentType: " application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({"cpf":cpf})
      }).done(
        function (data) {
          SpinnerDialog.hide();
          notificationAlert(
            "Uma nova senha foi enviada para o seu email",
            function() {},
            "Atenção",
            "OK"
          );
      }).fail(
        function(jqXHR, textStatus, errorThrown){
          SpinnerDialog.hide();
          notificationAlert(
            "Não foi possivel pesquisar, por favor tente novamente",
            function() {},
            "Atenção",
            "OK"
          );
        });
  },
  login: function(){
    SpinnerDialog.show();
    location.href = '../views/login.html';
    SpinnerDialog.hide();
  },
  pesquisar: function(){
    esqueciMinhaSenhaController.validar();
  },
  retornarEmail: function(){
    var cpf = removeCaracteres(document.getElementById('esqueci_minha_senha_input_text_cpf').value);
    var elementoEmail = document.getElementById('spnEmail');
    var labelEmail = document.getElementById('spnEmail');
    var textoEmailIncorreto = document.getElementById('container_texto_email_incorreto');

    SpinnerDialog.show();
    $.ajax(
      {
        url: config.wsUrl + "RetornarEmail",
        type: "POST",
        contentType: " application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({"cpf":cpf})
      }).done(
        function (data) {
          var dadosRetornados = JSON.parse( data );

          lblEmail.style.display = "block";
          textoEmailIncorreto.style.display = "block";
          btnRecuperar.style.display = "block";

          elementoEmail.innerHTML = dadosRetornados;
          SpinnerDialog.hide();
            //alert(data);
      }).fail(
        function(jqXHR, textStatus, errorThrown){
          elementoEmail.innerHTML = "";
          SpinnerDialog.hide();
          if(jqXHR.status == 404){
            notificationAlert(
              "O CPF enviado não foi encontrado",
              function() {},
              "Atenção",
              "OK"
            );

          }else{
            notificationAlert(
              "Não foi possivel pesquisar, por favor tente novamente",
              function() {},
              "Atenção",
              "OK"
            );
          }
        });
  }
};
